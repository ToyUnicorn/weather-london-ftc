import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WeatherLondonComponent} from './weather-london/weather-london.component';

const routes: Routes = [
  { path: 'weather', component: WeatherLondonComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingRoutingModule { }

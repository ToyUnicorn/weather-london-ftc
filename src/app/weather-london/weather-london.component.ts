import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as $ from 'jquery';
import {AngularGridInstance, Column, FieldType, Filters, Formatters, GridOption, GridState, GridStateChange} from 'angular-slickgrid';
import {FormControl, FormGroup} from '@angular/forms';
import {collectionSearchFilterCondition} from 'angular-slickgrid/app/modules/angular-slickgrid/filter-conditions/collectionSearchFilterCondition';
import {hasOwnProp} from 'ngx-bootstrap/chronos/utils/type-checks';

function randomBetween(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
const LOCAL_STORAGE_KEY = 'gridState';
const NB_ITEMS = 500;

@Component({
  selector: 'app-weather-london',
  templateUrl: './weather-london.component.html',
  styleUrls: ['./weather-london.component.scss']
})
export class WeatherLondonComponent implements OnInit {

  _PremiumApiBaseURL = 'http://api.worldweatheronline.com/premium/v1/';

  _PremiumApiKey = 'de6b5b0efd2d4e73b9381646192606';

  weatherLondon: any = {};

  formGroupWeather = new FormGroup({
    weather: new FormControl({}),
  });

  title = '';
  subTitle = `
    <ul class="small">
      <li>Необходимо получить и отобразить данные прогноза погоды в Лондоне на ближайшие 5 дней с
разбивкой по 3 часа.</li>
      <li>Данные, которые нужно отобразить:</li>
      <ul>
        <li>Дата</li>
        <li>Время</li>
        <li>Температура (ощущается как) в градусах Цельсия</li>
        <li>Влажность</li>
        <li>Давление</li>
        <li>Общее описание погоды</li>
      </ul>
      <li>Необходимо реализовать фильтрацию и сортировку полученных данных средствами SlickGrid.</li>
    </ul>
  `;

  angularGrid: AngularGridInstance;
  columnDefinitions: Column[];
  gridOptions: GridOption;
  dataset: any[];
  selectedLanguage: string;

  constructor(private translate: TranslateService) {
    this.selectedLanguage = this.translate.getDefaultLang();
  }

  angularGridReady(angularGrid: AngularGridInstance) {
    this.angularGrid = angularGrid;
  }

  ngOnInit(): void {
    this.defineGrid();

    this.formGroupObservable();

    const localWeatherInput = {
      query: 'London',
      format: 'JSON',
      num_of_days: '5',
      date: '',
      fx: '',
      cc: '',
      includelocation: '',
      show_comments: '',
      callback: 'LocalWeatherCallback'
    };

    this.JSONP_LocalWeather(localWeatherInput);
  }

   JSONP_LocalWeather(input) {
    const url = this._PremiumApiBaseURL + 'weather.ashx?q=' + input.query + '&format=' + input.format + '&extra=' + input.extra + '&num_of_days=' + input.num_of_days + '&date=' + input.date + '&fx=' + input.fx + '&cc=' + input.cc + '&includelocation=' + input.includelocation + '&show_comments=' + input.show_comments + '&key=' + this._PremiumApiKey;

    this.jsonP(url, input.callback);
  }

  jsonP(url, callback) {
    const _this = this;

    $.ajax({
      type: 'GET',
      url: url,
      async: true,
      contentType: 'application/json',
      jsonpCallback: callback,
      dataType: 'jsonp',
      success: function (json) {
        _this.formGroupWeather.controls.weather.patchValue(json);
      },
      error: function (e) {
        alert('Произошла ошибка при запросе!');
      }
    });
  }

  formGroupObservable(): void {
    this.formGroupWeather.valueChanges.subscribe(val => {
      if (val) {
        this.weatherLondon = val.weather.data;
        this.title = 'Погода в ' +  this.weatherLondon.request[0].query;
        this.dataset = [];
        let _id = 40;

        for (let indexDay = 0; indexDay < this.weatherLondon.weather.length; indexDay++) {
          for (let indexHour = 0; indexHour < 8; indexHour++) {
            _id -= 1;

            if (this.weatherLondon.weather[indexDay].hourly[indexHour].time.length === 1) {
              this.dataset[_id] = {
                id: _id,
                date: this.weatherLondon.weather[indexDay].date,
                time: this.weatherLondon.weather[indexDay].hourly[indexHour].time.charAt(0) + 0 + ':' + 0 + 0,
                tempC: this.weatherLondon.weather[indexDay].hourly[indexHour].tempC,
                humidity: this.weatherLondon.weather[indexDay].hourly[indexHour].humidity,
                pressure: this.weatherLondon.weather[indexDay].hourly[indexHour].pressure,
                weatherDesc: this.weatherLondon.weather[indexDay].hourly[indexHour].weatherDesc[0].value,
              };
            } else if (this.weatherLondon.weather[indexDay].hourly[indexHour].time.length === 3) {
                this.dataset[_id] = {
                  id: _id,
                  date: this.weatherLondon.weather[indexDay].date,
                  time: this.weatherLondon.weather[indexDay].hourly[indexHour].time.charAt(0)
                    + ':'
                    + this.weatherLondon.weather[indexDay].hourly[indexHour].time.charAt(1)
                    + this.weatherLondon.weather[indexDay].hourly[indexHour].time.charAt(2),
                  tempC: this.weatherLondon.weather[indexDay].hourly[indexHour].tempC,
                  humidity: this.weatherLondon.weather[indexDay].hourly[indexHour].humidity,
                  pressure: this.weatherLondon.weather[indexDay].hourly[indexHour].pressure,
                  weatherDesc: this.weatherLondon.weather[indexDay].hourly[indexHour].weatherDesc[0].value,
                };
            } else if (this.weatherLondon.weather[indexDay].hourly[indexHour].time.length === 4) {
                this.dataset[_id] = {
                  id: _id,
                  date: this.weatherLondon.weather[indexDay].date,
                  time: this.weatherLondon.weather[indexDay].hourly[indexHour].time.charAt(0)
                    + this.weatherLondon.weather[indexDay].hourly[indexHour].time.charAt(1)
                    + ':'
                    + this.weatherLondon.weather[indexDay].hourly[indexHour].time.charAt(2)
                    + this.weatherLondon.weather[indexDay].hourly[indexHour].time.charAt(3),
                  tempC: this.weatherLondon.weather[indexDay].hourly[indexHour].tempC,
                  humidity: this.weatherLondon.weather[indexDay].hourly[indexHour].humidity,
                  pressure: this.weatherLondon.weather[indexDay].hourly[indexHour].pressure,
                  weatherDesc: this.weatherLondon.weather[indexDay].hourly[indexHour].weatherDesc[0].value,
                };
            }
          }
        }
      }
    });
  }

  clearGridStateFromLocalStorage(): void {
    localStorage[LOCAL_STORAGE_KEY] = null;
    this.angularGrid.gridService.resetGrid(this.columnDefinitions);
  }

  defineGrid(gridStatePresets?: GridState): void {
    const multiSelectFilterArray = [];
    for (let i = 0; i < NB_ITEMS; i++) {
      multiSelectFilterArray.push({ value: i, label: i });
    }

    this.columnDefinitions = [
      {
        id: 'date',
        name: 'Дата',
        field: 'date',
        headerKey: 'DATE',
        filterable: true,
        sortable: true,
        type: FieldType.date,
        minWidth: 45, width: 100,
        filter: {
          model: Filters.compoundInput
        }
      },
      {
        id: 'time', name: 'Time', field: 'time', filterable: true, sortable: true, minWidth: 80, width: 100,
        type: FieldType.number,
        filter: {
          model: Filters.input
        }
      },
      {
        id: 'tempC', name: 'TempC', field: 'tempC', filterable: true, sortable: true, minWidth: 80, width: 100,
        type: FieldType.number,
        filter: {
          model: Filters.input
        }
      },
      {
        id: 'humidity', name: 'Humidity', field: 'humidity', sortable: true, type: FieldType.number, exportCsvForceToKeepAsString: true,
        minWidth: 55, width: 100,
        headerKey: 'HUMIDITY',
        filterable: true,
      },
      {
        id: 'pressure', name: 'Pressure', field: 'pressure', filterable: true, sortable: true, minWidth: 80, width: 100,
        type: FieldType.number,
        filter: {
          model: Filters.input
        }
      },
      {
        id: 'weatherDesc', name: 'WeatherDescription', field: 'weatherDesc', headerKey: 'weatherDesc', formatter: Formatters.dateIso, sortable: true, minWidth: 75, exportWithFormatter: true, width: 100,
        type: FieldType.string, filterable: true,
      },
    ];

    this.gridOptions = {
      autoResize: {
        containerId: 'demo-container',
        sidePadding: 15
      },
      enableCheckboxSelector: true,
      enableFiltering: true,
      enableTranslate: true,
      i18n: this.translate
    };

    if (gridStatePresets) {
      this.gridOptions.presets = gridStatePresets;
    }
  }

  /** Dispatched event of a Grid State Changed event */
  gridStateChanged(gridStateChanges: GridStateChange) {
    localStorage[LOCAL_STORAGE_KEY] = JSON.stringify(gridStateChanges.gridState);
  }

  /** Save current Filters, Sorters in LocaleStorage or DB */
  saveCurrentGridState(grid) {
    const gridState: GridState = this.angularGrid.gridStateService.getCurrentGridState();
    localStorage[LOCAL_STORAGE_KEY] = JSON.stringify(gridState);
  }
}

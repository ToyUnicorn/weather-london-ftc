import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherLondonComponent } from './weather-london.component';

describe('WeatherLondonComponent', () => {
  let component: WeatherLondonComponent;
  let fixture: ComponentFixture<WeatherLondonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherLondonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherLondonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
